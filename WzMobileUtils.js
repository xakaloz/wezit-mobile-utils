/**
 * This class purpose is to communicate between Wezit native applications and webviews.
 * 
 * requires joii : https://github.com/haroldiedema/joii
 */

/************* ALL THE CONSTANTS USED FOR COMMUNICATION BETWEEN NATIVE APP AND WEBVIEWS *************/

// Type of communication
var WzJsInterfaceType = Class({});

WzJsInterfaceType.SEND 		= "send";
WzJsInterfaceType.SET 		= "set";

// Event types
var WzJsInterfaceEvent = Class({});

WzJsInterfaceEvent.ABORT 	            = "event.abort";
WzJsInterfaceEvent.READY            	= "event.ready";
WzJsInterfaceEvent.CANCEL              	= "event.cancel";
WzJsInterfaceEvent.COMPLETE             = "event.complete";
WzJsInterfaceEvent.ORIENTATION_CHANGE   = "event.orientation_change";
WzJsInterfaceEvent.STARTED  			= "event.started";

// Data types
var WzJsInterfaceData = Class({});

WzJsInterfaceData.PATH 					= "data.path";
WzJsInterfaceData.STOP_ID				= "data.stop_id";
WzJsInterfaceData.LANG 					= "data.lang";

// Data keys
var WzJsInterfaceDataKey = Class({});

WzJsInterfaceDataKey.SCORE 				= "score";

var WzJsInterfaceOrientation = Class({});

WzJsInterfaceOrientation.TOP_UP		= "orientation.top_up";
WzJsInterfaceOrientation.BOTTOM_UP		= "orientation.bottom_up";
WzJsInterfaceOrientation.LEFT_UP		= "orientation.left_up";
WzJsInterfaceOrientation.RIGHT_UP		= "orientation.right_up";

/*********** END OF ALL THE CONSTANTS USED FOR COMMUNICATION BETWEEN NATIVE APP AND WEBVIEWS ************/


var WzMobileUtils = Class({});

WzMobileUtils.os = '';


/**
 * Determine the mobile operating system.
 * This function either returns 'ios', 'android' or undefined
 *
 * @returns {String}
 */
WzMobileUtils.getMobileOperatingSystem = function getMobileOperatingSystem()
{	
	if(WzMobileUtils.os === '')
	{
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;

		if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) ){
			WzMobileUtils.os = 'ios';
			JsNativeInterface = null;
			
		}else if( userAgent.match( /Android/i ) ){
			WzMobileUtils.os = 'android';

		}else{
			WzMobileUtils.os = undefined;
		}
	}
	return WzMobileUtils.os;
}

/**
 * Send command to native application from the webview
 * 
 * @param {[[Type]]} command [[Description]]
 * @param {[[Type]]} params  [[Description]]
 */
/**
 * Send command to native application from the webview
 * 
 * @param {String} type  Communication type, eg : set or send
 * @param {String} name  Type of event or data passed
 * @param {String} data
 */
WzMobileUtils.sendNative = function sendNative(type, name, data)
{
	if(type !== undefined)
	{
		if(WzMobileUtils.getMobileOperatingSystem() == "ios")
		{
			document.location.href = data !== undefined ? "#wezit:" + type + "^" + name + "^" + data.toString() : "#wezit:" + type + "^" + name;
		} else {
			if(typeof JsNativeInterface !== "undefined"){
				JsNativeInterface.communicate(type, name, data !== undefined ? data.toString() : null);
			} else {
				// Debug desktop
				alert("sendToNative => " + type + " : " + name + " : " + data);
			}
		}
	}
}


/**
 * Receive command from native application to the webview.
 * 
 * Dispatches an event to all the view.
 */
jsInterface = {
	communicate: function communicate(pType, pName, pData)
	{
		console.log("jsInterface => nativeToWebview : " + pType + " : " + pName + " : " + pData);
		window.dispatchEvent(new CustomEvent(WzJsInterfaceEvent.RECEPTION, {detail: { type: pType, name: pName, data: pData }}));
	}
}


// Polyfill if CustomEvent not supported
if(!window.CustomEvent) {
	var CustomEvent;

	CustomEvent = function(event, params) {
		var evt;
		params = params || {
			bubbles: false,
			cancelable: false,
			detail: undefined
		};
		evt = document.createEvent("CustomEvent");
		evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
		return evt;
	};

	CustomEvent.prototype = window.Event.prototype;

	window.CustomEvent = CustomEvent;
}
